#!/bin/sh
apt update
apt install zip
zip -r sn-solarized-light.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-solarized-light.zip public/
cd public
unzip sn-solarized-light.zip
