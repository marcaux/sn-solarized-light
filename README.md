# Solarized Light

This Theme is also within the offical Standard Notes Community Plugins repo in the app itself. I do my best to also keep that up to date with this repo.

If you always want to use the latest version (I sometimes tend to adjust a few little things here and there), you can use my deployed version here:

https://marcaux.gitlab.io/sn-solarized-light/ext.json

![Screenshot Standard Notes Theme Solarized Light](https://marcaux.gitlab.io/sn-solarized-light/screenshot.jpg "Screenshot Standard Notes Theme Solarized Light")
